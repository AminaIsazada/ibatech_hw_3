package com.company;

import java.util.Scanner;
public class Main {

    public static void main(String[] args) {

        //Step1-Filling the matrix
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "eat ice-cream";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "go to shopping";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "talk with friends";
        schedule[5][0] = "Friday";
        schedule[5][1] = "buy present for mom";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "listen to the music";


        //Step2-Getting weekday from user and printing the task of that entered day
        while (true) {
            Scanner scan = new Scanner(System.in);
            String weekday;
            System.out.println("Please, input the day of the week:");
            weekday = scan.nextLine();


            switch (weekday) {

                //Sunday
                case "Sunday":
                case "SUNDAY":
                case "sunday":
                case "Sunday ":
                case "SUNDAY ":
                    System.out.println("Your task for Sunday: " + schedule[0][1]);
                    System.out.println("------------------------");
                    break;

                //Monday
                case "Monday":
                case "MONDAY":
                case "monday":
                case "Monday ":
                case "MONDAY ":
                    System.out.println("Your task for Monday: " + schedule[1][1]);
                    System.out.println("------------------------");
                    break;

                //Tuesday
                case "Tuesday":
                case "TUESDAY":
                case "tuesday":
                case "Tuesday ":
                case "TUESDAY ":
                    System.out.println("Your task for Tuesday: " + schedule[2][1]);
                    System.out.println("------------------------");
                    break;

                //Wednesday
                case "Wednesday":
                case "WEDNESDAY":
                case "wednesday":
                case "Wednesday ":
                case "WEDNESDAY ":
                    System.out.println("Your task for Wednesday: " + schedule[3][1]);
                    System.out.println("------------------------");
                    break;

                //Thursday
                case "Thursday":
                case "THURSDAY":
                case "thursday":
                case "Thursday ":
                case "THURSDAY ":
                    System.out.println("Your task for Thursday: " + schedule[4][1]);
                    System.out.println("------------------------");
                    break;

                //Friday
                case "Friday":
                case "FRIDAY":
                case "friday":
                case "Friday ":
                case "FRIDAY ":
                    System.out.println("Your task for Friday: " + schedule[5][1]);
                    System.out.println("------------------------");
                    break;

                //Saturday
                case "Saturday":
                case "SATURDAY":
                case "saturday":
                case "Saturday ":
                case "SATURDAY ":
                    System.out.println("Your task for Saturday: " + schedule[6][1]);
                    System.out.println("------------------------");
                    break;

                case "Exit":
                case "EXIT":
                case "Exit ":
                case "EXIT ":
                case "exit":
                case "exit ":
                    System.exit(0);


                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }
}



